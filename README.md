
This is a simple linux/linux-mainline PKGBUILD to enable rapid regression testing against upstream stable changes

Usage:

```bash
mkdir <path/to/workspace>
cd <path/to/workspace>

# clone the linux-testing package files
git clone https://gitlab.com/arglebargle-arch/linux-regression-testing.git

# clone the affected kernel source:
git clone https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git
# or 
git clone https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git

# edit the linux-testing PKGBUILD to point the build system at your local tree:
cd <path/to/workspace>/linux-regression-testing
vim PKGBUILD

# comment out any remote source trees, eg:
#"$_srcname::git+https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git#tag=$_tag"

# uncomment and change this line to point to your path:
"$_srcname::git+file:///path/to/repository"

# eg:
"$_srcname::git+file:///home/arglebargle/arch-kernels/s0ix-linux-tree"

# start your git bisect in your cloned linux-stable tree:
cd <path/to/linux-stable-tree>
git bisect start

# tag the version where your problem started:
git bisect bad <new release with problems>
# eg: git bisect bad v5.16.3-rc1

# tag the most recent good version:
git bisect good <last known good tag>
# eg: git bisect good v5.16.2

# get the first bisect point
git bisect

# tag this point with something descriptive
git tag v5.16.3rc1-bisect01
# or
git tag mybisect01

# switch back to the linux-testing directory and change the build tag for the kernel package
cd <path/to/workspace>/linux-regression-testing

_tag=v5.16
# becomes
_tag=v5.16.3rc1-bisect01

# edit the pkgver
pkgver=5.16
# becomes
pkgver=5.16.3rc1.bisect01

# build, install and test your package

# mark your source tree with the results
cd <path/to/linux-stable-tree>
# good test
git bisect good
# bad result
git bisect bad

# tag your next point
git tag v5.16.3rc1-bisect02

# edit your kernel package sources
_tag=v5.16.3rc1-bisect02
pkgver=5.16.3rc1.bisect02

# repeat building and testing until you've located the problem

# report your results upstream to the stable kernel mailing list or the subsystem maintainer
# if you're testing against mainline

```
